

$('.member_text').hide().first().show();
$('.member_name').hide().first().show();
$('.member_position').hide().first().show();
$('.center_image').hide().first().show();

$('.thumbnail_img').on('click', function () {
    $(this).addClass('active').siblings().removeClass('active');
    changeInfo();
});

$('.slider_arrow').on('click', function () {
    if ($(this).hasClass('left')) {
        const activeIndex = $('.thumbnail_img.active').index();
        const newIndex = (activeIndex === 0) ? 3 : activeIndex - 1;

        $(`.thumbnail_img:eq(${newIndex})`).addClass('active').siblings().removeClass('active');

        changeInfo();
    }
    if ($(this).hasClass('right')) {
        const activeIndex = $('.thumbnail_img.active').index();
        const newIndex = (activeIndex === 3) ? 0 : activeIndex + 1;

        $(`.thumbnail_img:eq(${newIndex})`).addClass('active').siblings().removeClass('active');

        changeInfo();
    }
});

function changeInfo() {
    const activeIndex = $('.thumbnail_img.active').index();

    $(`.member_text:eq(${activeIndex})`).fadeIn().siblings('.member_text').hide();

    $(`.member_name:eq(${activeIndex})`).fadeIn().siblings('.member_name').hide();

    $(`.member_position:eq(${activeIndex})`).fadeIn().siblings('.member_position').hide();

    $(`.center_image:eq(${activeIndex})`).fadeIn().siblings('.center_image').hide();
}



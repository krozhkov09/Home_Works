

let menu =[...$(".our_service_menu_item")];
let content = [...$(".our_service_content")];
let activeIndex = 0;

$("#service_menu").on("click", function (event) {
    $(menu[activeIndex]).removeClass("active");
    $(content[activeIndex]).removeClass("active");

    activeIndex = $.inArray(event.target, menu);

    $(menu[activeIndex]).addClass("active");
    $(content[activeIndex]).addClass("active");
});
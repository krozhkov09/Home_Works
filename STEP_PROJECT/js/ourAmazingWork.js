

$( document ).ready(function () {
    $(".our_amazing_work_content_item").hide();
    $(".load_img").slice(0, 12).show();
    if ($(".add_img:hidden").length !== 0) {
        $(".our_amazing_work_button_load").show();
    }
    $(".our_amazing_work_button_load").on('click', function (e) {
        e.preventDefault();
        $(".load_img:hidden").slice(0, 12).slideDown();
        if ($(".load_img:hidden").length === 0) {
            $(".our_amazing_work_button_load").fadeOut('slow');
        }
    });
    $('.our_amazing_work_menu_item').click(function() {
        $(this).addClass('selected').siblings().removeClass("selected");
        const imageType = $(this).data("filter");
        $(".our_amazing_work_content_item").hide();
        $(`.our_amazing_work_content_item${imageType}`).slice(0,12).show();

    });
});

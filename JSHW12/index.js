let activeIndex = 0;
const imgItems = [...document.getElementsByClassName("image-to-show")];
function show() {
    fadeOut(imgItems[activeIndex]);
    activeIndex++;
    if (activeIndex >= imgItems.length) {
        activeIndex = 0;
    }
    fadeIn(imgItems[activeIndex]);
}

function countdown() {
    milliseconds.innerText--;
    if (milliseconds.innerText <= 0) {
        seconds.innerText--;
        milliseconds.innerText = "100";
    }
    if(seconds.innerText <= 0){
        seconds.innerText = "9";
        show()
    }
}
let seconds = document.getElementById("clock");
let countdownInterval = setInterval(countdown, 10);
let lock = true;

document.getElementById("stop").addEventListener("click", function () {
    clearInterval(countdownInterval);
    lock = false;
});

document.getElementById("go-on").addEventListener("click", function () {
    if (!lock) {
        lock = true;
        countdownInterval = setInterval(countdown, 10);
    }

});
function fadeIn(element) {
    element.style.opacity = parseFloat(element.style.opacity || 0) + 0.5;
    if (!(parseFloat(element.style.opacity) > 1.0)) {
        setTimeout(() => fadeIn(element), 100);
    }
}
function fadeOut(element) {
    element.style.opacity = parseFloat(element.style.opacity || 1) - 0.5;
    if (!(parseFloat(element.style.opacity) < 0.0)) {
        setTimeout(() => fadeOut(element), 100);
    }
}



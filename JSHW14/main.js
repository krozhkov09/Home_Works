let menu =[...$(".tabs-title")];
let content = [...$(".text")];
let activeIndex = 0;

$("#tabs-menu").on("click", function (event) {
    $(menu[activeIndex]).removeClass("active");
    $(content[activeIndex]).removeClass("active");

    activeIndex = $.inArray(event.target, menu);

    $(menu[activeIndex]).addClass("active");
    $(content[activeIndex]).addClass("active");
});
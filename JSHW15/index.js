
$("a").click(function(){
    let element = $(this).attr("href");
    let destination = $(element).offset().top;
    $('html, body').animate({ scrollTop: destination }, 2000);
});
$('body').append('<button id="top">UP</button>');
$('#top').css({visibility:'hidden',width:100,height:40,position:'fixed',right:10,top:500,
    background:'lightgray',border:1, cursor:'pointer'});

$(window).scroll(function(){
    if($(window).scrollTop() > 550){
        $('#top').css('visibility','visible');
    }

    if($(window).scrollTop() === 0){
        $('#top').css('visibility','hidden');
    }
});
$('#top').click(function(){
    $('html, body').animate({ scrollTop: 0 }, 2000)
});

$('#btnSlide').click(function(){
    $('#news').slideToggle('fast')
});
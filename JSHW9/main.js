let menu = document.getElementById("tabs-menu");
let menuItems = [...document.getElementsByClassName("tabs-title")];
let content = [...document.getElementsByClassName("text")];

let activeIndex = 0;

menu.addEventListener("click", function (event) {
    menuItems[activeIndex].classList.remove("active");
    content[activeIndex].classList.remove("active");

    activeIndex = menuItems.indexOf(event.target);

    menuItems[activeIndex].classList.add("active");
    content[activeIndex].classList.add("active");
});
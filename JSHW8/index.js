
function handleInputFocus(e) {
    priceInput.className = "price";
    this.classList.add("input-border");
    const error = document.getElementById("error-message");
    if (!error.classList.contains("d-none")) {
        error.classList.add("d-none");
    }

}

function handleInputBlur(e) {
    const inputValue = priceInput.value;
    if (!inputValue || isNaN(inputValue) || +inputValue <= 0 ) {
        priceInput.classList.add("Error");
        document.getElementById("error-message").classList.remove("d-none");
        return;
    }
    this.classList.remove("input-border");
    currPrice.classList.remove("d-none");
    const div = document.createElement("div");
    const span = document.createElement("span");
    span.innerHTML = `Current price: ${inputValue} $` ;
    const btn = document.createElement("button");
    btn.innerText = "X";
    btn.addEventListener("click", function (e) {
        div.remove();
        priceInput.value = "";
    });
    div.classList.add("currPrice");
    div.appendChild(span);
    div.appendChild(btn);
    btn.classList.add("buttonStyle");
    priceInput.classList.add("valueInput");
    currPrice.appendChild(div);
    console.log();

}

priceInput.addEventListener('focus', handleInputFocus);
priceInput.addEventListener('blur', handleInputBlur);


